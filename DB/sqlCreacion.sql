-- Creacion de la base de datos
create database ecotico;
create user 'ecotico'@'%' identified by 'EcoTico2019*';
grant all privileges on ecotico.* to 'ecotico'@'%';
create table carousel(
    idCarousel int primary key auto_increment,
    img varchar(100)
);

insert into carousel (img) values ('c_1.jpg'),('c_2.jpg'),('c_3.jpg');

create table Usuario(
	idUsuario int primary key auto_increment,
    cedula varchar(11),
    usuario varchar(50) not null,
    contra varchar(250) not null,
    nombre varchar(50),
    telefono varchar(15)
);

create table Actividad(
    idActividad int primary key auto_increment,
    nombre varchar(50) not null,
    descripcion varchar(255) default 'Sin descripcion',
    fecha date,
    localizacion varchar(200),
    img varchar(100)
);

create table Usuario_Actividad(
    idUsuario int,
    idActividad int,
    constraint idUsuario_idActividad_Usuario_Actividad_pk primary key(idUsuario,idActividad),
    constraint idUsuario_Usuario_Actividad_fk foreign key(idUsuario) references Usuario(idUsuario),
    constraint idActividad_Usuario_Actividad_fk foreign key(idActividad) references Actividad(idActividad)
);
