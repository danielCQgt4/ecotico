<?php
include '../Models/models.php';
$session = new Session();
$dataGET = $_GET;
$dataPOST = $_POST;

if (isset($dataGET['img']) && $dataGET['img'] == '1') {
    echo '[{"img":"c_1.jpg"},{"img":"c_2.jpg"},{"img":"c_3.jpg"}]';
} else if (isset($dataPOST['login']) && $dataPOST['login'] == '1') {
    $value = $dataPOST['user'] == 'ecotico' && $dataPOST['pass'] == 'ecotico' ? '1' : '0';
    if ($dataPOST['user'] == 'ecotico' && $dataPOST['pass'] == 'ecotico') {
        $session->setUser($dataPOST['pass'], $dataPOST['user']);
        $session->setActive('1');
    } else {
        $session->setActive('0');
    }
    echo '{"accept":' . $session->getActive() . '}';
} else if (isset($dataPOST['actividad']) && $dataPOST['actividad'] == '1') {
    if ($session->getActive() == '1') {
        echo '[
            {
                "title":"Recolectar basura",
                "description":"Ahora puedes ayudar al planeta recogiendo basura con GreenWolf",
                "date":"25/11/2019",
                "type":1,
                "img":"a_1.jpg"
            },
            {
                "title":"Limpieza ambientar",
                "description":"Ayuda a mejorar el ecosistema de la vencidad de Santa Marta",
                "date":"28/11/2019",
                "type":0,
                "img":"a_2.jpg"
            }
            ]';
    } else {
        echo '[]';
    }
} else if (isset($dataGET['close']) && $dataGET['close'] == '1') {
    $session->closeSession();
    header('Location: http://' . $_SERVER['HTTP_HOST']);
} else {
    echo '{"error":"Api went wrong"}';
}
