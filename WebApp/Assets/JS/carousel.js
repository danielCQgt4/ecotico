(function () {
    'use-stric';
    var carouselP = document.querySelector('#cyc-carousel-item-p');
    var carouselList = document.querySelector('#cyc-carousel-list');
    var maxCarousel = 0, curCarousel;
    var btnNext = document.querySelector('#cyc-btn-carousel-next');
    var btnPrevious = document.querySelector('#cyc-btn-carousel-previous');

    getAjaxRequest('../API/api.php', '?img=1', function (result) {
        if (result != 'Error') {
            var json = JSON.parse(result);
            maxCarousel = json.length;
            for (i = 0; i < maxCarousel; i++) {
                var itemDiv = document.createElement('div');
                var imgDiv = document.createElement('div');
                var carouselPoint = document.createElement('li');
                //Div de la imagen en el carousel
                imgDiv.setAttribute('style', 'background: url(../Assets/IMG/' + json[i].img + ') no-repeat;background-attachment: inherit;background-size: 100% 100%;');
                imgDiv.setAttribute('class', 'cyc-carousel-img');
                //Div padre de la imagen
                itemDiv.setAttribute('id', 'cyc-carousel-item-' + (i + 1));
                itemDiv.setAttribute('class', 'cyc-carousel-item');
                itemDiv.setAttribute('style', 'display: none;');
                //Puntos en la lista
                carouselPoint.setAttribute('id', 'cyc-carousel-i-' + (i + 1));
                carouselPoint.setAttribute('class', 'cyc-carousel-indicator');
                carouselPoint.addEventListener('click', function () {
                    agregarEventoPunto(this);
                });
                //Appends
                itemDiv.appendChild(imgDiv);
                carouselP.insertBefore(itemDiv, carouselP.childNodes[i]);
                carouselList.insertBefore(carouselPoint, carouselList.childNodes[i]);
            }
            initCarousel();
        }
    });

    function initCarousel() {
        if (maxCarousel > 0) {
            curCarousel = 1;
            document.querySelector('#cyc-carousel-i-1').setAttribute('class', 'cyc-carousel-indicator cyc-carousel-indicator-active');
            document.querySelector('#cyc-carousel-item-1').setAttribute('style', 'display: block;');
        }
    }

    function moveCarousel(tipo) {
        var temp;
        if (typeof tipo === 'boolean') {
            temp = tipo ? curCarousel + 1 : curCarousel - 1;
            if (temp <= 0 || temp > maxCarousel) {
                temp = temp <= 0 ? maxCarousel : 1;
            }
        } else {
            temp = tipo;
        }
        toggle(document.querySelector('#cyc-carousel-item-' + curCarousel));
        document.querySelector('#cyc-carousel-i-' + curCarousel).setAttribute('class', 'cyc-carousel-indicator');
        document.querySelector('#cyc-carousel-i-' + temp).setAttribute('class', 'cyc-carousel-indicator cyc-carousel-indicator-active');
        toggle(document.querySelector('#cyc-carousel-item-' + temp));
        curCarousel = temp;
    }

    btnNext.addEventListener('click', function () {
        moveCarousel(true);
    });
    btnPrevious.addEventListener('click', function () {
        moveCarousel(false);
    });

    setInterval(function () {
        moveCarousel(true);
    }, 10000);

    function agregarEventoPunto(item) {
        var num = item.id.replace('cyc-carousel-i-', '');
        moveCarousel(num);
    };


})();