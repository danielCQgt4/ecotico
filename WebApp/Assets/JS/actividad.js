(function () {
    "use-stric";
    var toDo = document.querySelector("#actividad-to-do");
    var other = document.querySelector("#actividad-others");

    function getDOMObject(data) { //tipo?yo lo hago:otros
        if (data.type == '1') {
            return getDOM(data, 'Cancelar', 'btn-danger');
        } else {
            return getDOM(data, 'Participar', 'btn-success');
        }
    }

    function getDOM(data, btnText, btnType) {
        var actividad = document.createElement('div');
        var actividadImg = document.createElement('div');
        var actividadInfo = document.createElement('div');
        var actividadTitulo = document.createElement('div');
        var actividadDescripcion = document.createElement('div');
        var actividadFecha = document.createElement('div');
        var actividadBtn = document.createElement('div');
        var input = document.createElement('input');
        /* actividad-btn*/
        input.setAttribute('type', 'button');
        input.setAttribute('Value', btnText);
        input.setAttribute('class', 'btn ' + btnType + ' m-0 f-right actividad-btn');
        actividadBtn.setAttribute('class', 'actividad-btns');
        actividadBtn.appendChild(input);
        /* actividad-info */
        actividadFecha.setAttribute('class', 'actividad-fecha');
        actividadFecha.appendChild(document.createTextNode('Fecha: ' + data.date));
        actividadDescripcion.setAttribute('class', 'actividad-descripcion');
        actividadDescripcion.appendChild(document.createTextNode(data.description));
        actividadTitulo.setAttribute('class', 'actividad-titulo');
        actividadTitulo.appendChild(document.createTextNode(data.title));
        actividadInfo.setAttribute('class', 'actividad-info');
        actividadInfo.appendChild(actividadTitulo);
        actividadInfo.appendChild(actividadDescripcion);
        actividadInfo.appendChild(actividadFecha);
        /* actividad-img */
        actividadImg.setAttribute('class', 'actividad-img');
        actividadImg.setAttribute('style', 'background: url(../Assets/IMG/' + data.img + ') no-repeat; background-position: center center; background-attachment: inherit; background-size: 100% 100%;');
        /* actividad */
        actividad.setAttribute('class', 'actividad p-1');
        actividad.appendChild(actividadImg);
        actividad.appendChild(actividadInfo);
        actividad.appendChild(actividadBtn);
        return actividad;
    }

    postAjaxRequest("../API/api.php", "actividad=1", function (result) {
        if (result != "Error") {
            console.log(result);
            var json = JSON.parse(result);
            console.log(json[0].title);
            for (i = 0; i < json.length; i++) {
                var actividad = getDOMObject(json[i]);
                if (json[i].type == '1') {
                    toDo.appendChild(actividad);
                } else {
                    other.appendChild(actividad);
                }
            }
        }
    });
})();