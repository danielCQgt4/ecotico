<?php
include './Partials/headerClient.php';
if (!empty($_SESSION['active'])) {
    if ($session->getActive() == '1') {
        ?>
        <div class="box-actividades p-1">
            <h2 class="mt-1">Actividades</h2>

            <div class="actividades" id="actividad-to-do">
                <div class="title-control mt-1 p-1">
                    <h6 class="d-inline-block">Actividades que participo</h6>
                    <div class="btn-control-actividades d-inline-block ">
                        +
                    </div>
                </div>

                <!-- LISTA DE ACTIVIDADES -->

            </div>

            <div class="actividades" id="actividad-others">
                <div class="title-control mt-1 p-1">
                    <h6 class="d-inline-block">Otras actividades</h6>
                    <div class="btn-control-actividades d-inline-block ">
                        -
                    </div>
                </div>

                <!-- LISTA DE ACTIVIDADES -->

            </div>
        </div>
<?php
    } else {
        header('Location: http://' . $_SERVER['HTTP_HOST']);
    }
} else {
    header('Location: http://' . $_SERVER['HTTP_HOST']);
}
include './Partials/footerClient.php';
?>
<script src="http://<?= $_SERVER['HTTP_HOST'] ?>/Assets/JS/actividad.js"></script>