<?php
include './Partials/headerClient.php';
?>
<div class="cyc-carousel text-white">
    <div class="cyc-carousel-content">
        <div class="cyc-carousel-inner" id="cyc-carousel-item-p">
            <input type="button" title="Previous" id="cyc-btn-carousel-previous" class="cyc-btn-carousel cyc-btn-carousel-previous">
            <input type="button" title="Next" id="cyc-btn-carousel-next" class="cyc-btn-carousel cyc-btn-carousel-next">
            <div class="cyc-div-carousel-indicators">
                <ul class="cyc-carousel-indicators" id="cyc-carousel-list"></ul>
            </div>
        </div>
    </div>
</div>
<h2 class="mt-1 mb text-center">Contaminación en Costa Rica</h2>
<hr class="m-1">
<p class="text-size-px-20 m-1 " style="text-align: justify;">
    Costa Rica es un país bendecido por la naturaleza, nuestro estilo de vida “amigable con el ambiente” es lo que llama la atención de extranjeros, pero ¿Realmente es amigable? Los habitantes tienen un estilo de vida completamente diferente al que todos piensan, aquí la palabra “desechable” es un regalo, no hay que lavar se bota y listo, no interesa saber que pasará más adelante, como si desapareciera por arte de magia cuando pasa el camión de la basura. Los ticos son super consumistas, todo lo que ven si es bonito y barato lo compran, ese pensamiento de “quiero todo” es el que nos lleva a desechar muchísimas cosas sin pensar en como reutilizarla, reciclarla, sacarle provecho de alguna manera. Un estudio de la UCR habla sobre este tema, el mayor problema ambiental es el consumo del país. Un alto porcentaje de la población no separa sus residuos, un alto porcentaje tira residuos a la calle. La capital del país es un completo basurero.
    Es necesario tomar conciencia y cambiar hoy mismo, trabajar en mis hábitos de consumo y ver como puedo contribuir en diferentes actividades que ayudan a que Costa Rica sea un país realmente verde.
</p>
<div class="img-index border-r-17"></div>
<?php
include './Partials/footerClient.php';
?>
<script src="http://<?= $_SERVER['HTTP_HOST']; ?>/Assets/JS/carousel.js"></script>