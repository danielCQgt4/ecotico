<?php
include './Partials/headerClient.php';
?>

<div class="mt-2 box-center-h box-acerca text-accent border-solid border-w-1 w-90 mb-10">
    <h2 class="text-left ">Acerca de EcoTico</h2>
    <hr class="mt-1 mb">
    <p class="text-accent text-size-px-20">Costa Rica se enorgullece por ser “El país más verde del mundo” pero ¿realmente
        lo es? La educación ambiental no es prioridad en el país, por lo tanto ¿Qué podemos
        hacer? Si demostramos a los ciudadanos la importancia de cuidar nuestra huella
        ecología se puede llegar a ser realmente un país verde. Por medio de la tecnología
        se puede llamar la atención tanto de niños como adultos para crear la conciencia
        que tanto necesitamos, todo esto por medio de una aplicación que demostrar todas
        las acciones que podemos realizar desde nuestros hogares para que nuestra huella
        ecológica sea cada vez más verde.</p>
    <h4 class="mt-2 mb">Integrantes</h4>
    <h6 class="m-1">Tiffany Avendaño Rodríguez</h6>
    <h6 class="m-1">Daniel Coto Quiros</h6>
    <h6 class="m-1">Alexander Díaz Rojas</h6>
    <h6 class="m-1">Jean Carlo Núñez Jiménez</h6>
    <h6 class="m-1">Alonso Quirós Quesada</h6>
    <h6 class="m-1">Alexa Solís González</h6>
</div>
<?php
include './Partials/footerClient.php';
?>