<?php
include '../Models/models.php';
include '../Running/generalFunction.php';
$session = new Session();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST']; ?>/Assets/CSS/cycStyles.css">
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST']; ?>/Assets/CSS/general.css">
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST']; ?>/Assets/CSS/ecotico.css">
    <link rel="stylesheet" href="http://<?= $_SERVER['HTTP_HOST']; ?>/Assets/CSS/carousel.css">
    <link rel="shortcut icon" href="http://<?= $_SERVER['HTTP_HOST']; ?>/Assets/IMG/favicon.png" type="image/x-icon">
    <title>EcoTico</title>
</head>

<body class="body">
    