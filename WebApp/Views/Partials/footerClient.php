<nav class="nav text-white">
    <a href="http://<?= $_SERVER['HTTP_HOST']; ?>" class="nav-title">EcoTico</a>
    <ul class="nav-list" id="nav-list" style="display: none;">
        <li class="nav-item"><a href="http://<?= $_SERVER['HTTP_HOST']; ?>" class="d-block text-white w-100 h-100">Inicio</a></li>
        <li class="nav-item"><a href="http://<?= $_SERVER['HTTP_HOST']; ?>/Views/acerca.php" class="d-block text-white w-100 h-100">Acerca</a></li>
        <?php
        $url;
        $text;
        if (!empty($_SESSION['active'])) {
            $url = $session->getActive() == '1' ? 'http://' . $_SERVER['HTTP_HOST'] . '/Views/actividad.php' : 'http://' . $_SERVER['HTTP_HOST'] . '/Views/login.php';
        } else {
            $url = 'http://' . $_SERVER['HTTP_HOST'] . '/Views/login.php';
        }
        if (!empty($_SESSION['active'])) {
            $text = $session->getActive() == '1' ? 'Actividades' : 'Iniciar sesion';
        } else {
            $text = 'Inciar sesion';
        } ?>
        <li class="nav-item"><a href="<?= $url ?>" class=" d-block text-white w-100 h-100"><?= $text ?></a></li>
        <?php
        $url;
        $text;
        if (!empty($_SESSION['active'])) {
            $url = $session->getActive() == '1' ? 'http://' . $_SERVER['HTTP_HOST'] . '/API/api.php?close=1' : 'http://' . $_SERVER['HTTP_HOST'] . '/Views/registro.php';
        } else {
            $url = 'http://' . $_SERVER['HTTP_HOST'] . '/Views/registro.php';
        }
        if (!empty($_SESSION['active'])) {
            $text = $session->getActive() == '1' ? 'Cerrar sesion' : 'Registrarse';
        } else {
            $text = 'Registrarse';
        } ?>
        <li class="nav-item"><a href="<?= $url ?>" class=" d-block text-white w-100 h-100"><?= $text ?></a></li>
    </ul>
    <div class="btn-action" id="btn-action">
    </div>
</nav>

<footer class="footer text-white p-4">
    <div class="text-center mt-1">
        <p>EcoTico@cycwebservice.cf</p>
    </div>
</footer>

<script src="http://<?= $_SERVER['HTTP_HOST']; ?>/Assets/JS/general.js"></script>
</body>

</html>